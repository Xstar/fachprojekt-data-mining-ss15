package core;

import utils.Conditions;
import utils.Indexing;
import utils.Sentence;
import utils.Tuple;

import java.lang.Math;
import java.util.HashMap;

import core.GradientDescent;

public class ForwardBackward {

    /**
     * The Gradient Descent Object
     */
    private GradientDescent model;

    /**
     * The current sequence
     */
    private Sentence sequence;

    /**
     * The weights of the model
     */
    private HashMap<Integer, Double> theta;

    /**
     * The Alpha probabilities calculated by the forward Algorithm
     */
    private double[][] forwardProbabilities;

    /**
     * The Beta probobailities calculated by the backward Algorithm
     */
    private double[][] backwardProbabilities;

    private int sequence_length;

    private double[][] probabilities;

    private double[] scales;

    /**
     * Create a new probabilty calculator for the given Gradien Descent Object
     * and the current sentence
     * 
     * @param model
     * @param sentence
     */
    public ForwardBackward(GradientDescent model, Sentence sentence) {
	this.model = model;
	this.sequence = sentence;
	this.sequence_length = sentence.getEncodedSentence().size();
	this.forwardProbabilities = new double[model.getModelStates()][sequence_length];
	this.backwardProbabilities = new double[model.getModelStates()][sequence_length];
	this.theta = new HashMap<>();
	this.probabilities = new double[model.getModelStates()][sequence_length];
    }

    /**
     * Calculate and set the alpha values by recursively computing the
     * probabilities of each label
     */
    public void forward() {
	double sum = 0;
	for (int i = 0; i < model.getModelStates(); i++) {
	    // intial probabilites ?
	    this.forwardProbabilities[i][0] = this.theta.get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T,
		    this.sequence.getEncodedSentence().get(0).getWord(), i));

	}
	this.scales = new double[sequence_length];
	sum += this.forwardProbabilities[0][0] + this.forwardProbabilities[1][0];
	if (sum > 0.0) {
	    scales[0] = (double) 1 / sum;
	} else {
	    scales[0] = 1;
	}

	for (int i = 0; i < model.getModelStates(); i++) {
	    forwardProbabilities[i][0] *= scales[0];
	}

	for (int i = 1; i < this.sequence_length; i++) {
	    for (int q = 0; q < model.getModelStates(); q++) {
		double tmp_sum = 0.0;
		for (int r = 0; r < model.getModelStates(); r++) {
		    tmp_sum += this.forwardProbabilities[r][i - 1]
			    * this.forwardProbability(q, this.sequence.getEncodedSentence().get(i), i);

		}
		this.forwardProbabilities[q][i] = tmp_sum;
	    }
	    sum = 0;
	    for (int j = 0; j < model.getModelStates(); j++) {
		sum += forwardProbabilities[j][i];
	    }
	    if (sum > 0.0) {
		scales[i] = 1.0 / sum;
	    } else {
		scales[i] = 1;
	    }
	    for (int j = 0; j < model.getModelStates(); j++) {
		forwardProbabilities[j][i] *= scales[i];
	    }
	}
    }

    private double forwardProbability(int state, Tuple<Integer, Integer> tuple, int pos) {
	double sum = 0.0;
	Tuple<Integer, Integer> current = this.sequence.getEncodedSentence().get(pos);
	Tuple<Integer, Integer> previous = this.sequence.getEncodedSentence().get(pos - 1);

	// theta(y_i-1,y_i)
	double theta_trans = this.theta.get(model.getIndex().getIndexOf(0, previous.getLabel(), state));
	// theta(x_i-1,q)
	double theta_prev = this.theta
		.get(model.getIndex().getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous.getWord(), state));
	// theta(x_i,q)
	double theta_current = this.theta
		.get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, current.getWord(), state));
	// theta (x_i-1,y_i-1)
	//double theta_prev_label = this.theta
	//	.get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, previous.getWord(), previous.getLabel()));

	sum = theta_trans + theta_current + theta_prev;  //theta_prev_label
	if (sum != 0.0) {
	    return Math.exp(sum);
	}
	return Math.exp(sum);
    }

    public void backward() {
	for (int i = 0; i < this.model.getModelStates(); i++) {
	    this.backwardProbabilities[i][this.sequence_length - 1] = 1 * forwardProbabilities[i][sequence_length - 1];
	}

	for (int pos = this.sequence_length - 2; pos >= 0; pos--) {
	    for (int state = 0; state < model.getModelStates(); state++) {
		double tmp_sum = 0.0;
		for (int inner_state = 0; inner_state < model.getModelStates(); inner_state++) {
		    tmp_sum += this.backwardProbabilities[inner_state][pos + 1] * this.backwardProbability(state, pos);

		}
		this.backwardProbabilities[state][pos] = tmp_sum * scales[pos];
	    }
	}

    }

    private double backwardProbability(int state, int pos) {
	double sum = 0.0;
	Tuple<Integer, Integer> current = this.sequence.getEncodedSentence().get(pos);
	Tuple<Integer, Integer> next = this.sequence.getEncodedSentence().get(pos + 1);
	// theta(q,y_i+1)
	double theta_trans = this.theta.get(model.getIndex().getIndexOf(0, state, next.getLabel()));
	// theta (x_i+1,y_i+1)
	//double theta_next = this.theta
	//	.get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, next.getWord(), next.getLabel()));
	// theta (x_i.y_i+1)
	double theta_cross = this.theta
		.get(model.getIndex().getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, current.getWord(), next.getLabel()));
	sum = theta_trans +  theta_cross; //theta_next +
	return Math.exp(sum);
    }

    public void set_Phi_Theta() {
	for (int j = 0; j < 2; j++) {
	    for (int k = 0; k < 2; k++) {
		int index = model.getIndex().getIndexOf(0, j, k);
		this.theta.put(index, model.getTheta().get(index));
	    }

	}
	for (Tuple<Integer, Integer> tuple : this.sequence.getEncodedSentence()) {
	    for (int i = 1; i <= model.getObservation_rules(); i++) {
		int index = model.getIndex().getIndexOf(i, tuple.getWord(), 0);
		int index_compl = model.getIndex().getIndexOf(i, tuple.getWord(), 1);
		if (!this.theta.containsKey(index)) {
		    this.theta.put(index, model.getTheta().get(index));
		}
		if (!this.theta.containsKey(index_compl)) {
		    this.theta.put(index_compl, model.getTheta().get(index_compl));
		}
	    }
	}
    }

    public void probability() {

	for (int pos = 0; pos < this.sequence_length; pos++) {
	    double sum = forwardProbabilities[0][pos] * backwardProbabilities[0][pos]
		    + forwardProbabilities[1][pos] * backwardProbabilities[1][pos];
	    if (sum != 0.0) {
		this.probabilities[0][pos] = (forwardProbabilities[0][pos] * backwardProbabilities[0][pos]) / sum;
		this.probabilities[1][pos] = (forwardProbabilities[1][pos] * backwardProbabilities[1][pos]) / sum;
	    } else {
		this.probabilities[0][pos] = 0.0;
		this.probabilities[1][pos] = 0.0;
	    }
	}
    }

    public Sentence getSequence() {
	return sequence;
    }

    public double[][] getForwardProbabilities() {
	return forwardProbabilities;
    }

    public double[][] getBackwardProbabilities() {
	return backwardProbabilities;
    }

    public void setGd(GradientDescent gd) {
	this.model = gd;
    }

    public double[][] getProbabilities() {
	return this.probabilities;
    }

    public void execute() {
	this.set_Phi_Theta();
	this.forward();
	this.backward();
	// System.out.println("FORWARD");

	// for (int i = 0; i < model.getModelStates(); i++) {
	// for (int j = 0; j < sequence_length; j++) {
	// System.out.print(this.forwardProbabilities[i][j] + " ");
	// }
	// System.out.println();
	// }
	// System.out.println("BACKWARD");
	// for (int i = 0; i < model.getModelStates(); i++) {
	// for (int j = 0; j < sequence_length; j++) {
	// System.out.print(this.backwardProbabilities[i][j] + " ");
	// }
	// System.out.println();
	// }

	this.probability();

//	System.out.println("PROBABILITY");
//	for (int i = 0; i < model.getModelStates(); i++) {
//	    for (int j = 0; j < sequence_length; j++) {
//		System.out.print(this.probabilities[i][j] + " ");
//	    }
//	    System.out.println();
//	}
	// int i = 0;
	// System.out.println(i);
    }

    public double getTransitionProbability(int position, int previous_position, int label, int previous_label) {
	Indexing index = this.model.getIndex();
	double transition = Math
		.exp(this.theta.get(index.getIndexOf(Conditions.NP_T_MINUS_1_AND_NP_T, previous_label, label))
			+ this.theta.get(index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T,
				sequence.getTuple(previous_position).getWord(), label))
		+ this.theta
			.get(index.getIndexOf(Conditions.X_T_AND_NP_T, sequence.getTuple(position).getWord(), label)));

	double sum = 0.0;
	for (int outer_label = 0; outer_label < model.getModelStates(); outer_label++) {
	    for (int inner_label = 0; inner_label < model.getModelStates(); inner_label++) {
		sum += this.forwardProbabilities[outer_label][previous_position]
			* this.backwardProbabilities[inner_label][position] * transition;
	    }
	}

	if (sum == 0.0) {
	    return 0.0;
	}

	double probability = (this.forwardProbabilities[previous_label][previous_position]
		* this.backwardProbabilities[label][position] * transition) / sum;
	
	return probability;
    }

    public int[] maxForward() {
	double[][] labelmap = new double[model.getModelStates()][sequence.size() * 2];

	for (int i = 0; i < model.getModelStates(); i++) {
	    // intial probabilites ?
	    if (this.theta.containsKey(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T,
		    this.sequence.getEncodedSentence().get(0).getWord(), i))) {
		this.forwardProbabilities[i][0] = this.theta.get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T,
			this.sequence.getEncodedSentence().get(0).getWord(), i));
	    } else {
		this.forwardProbabilities[i][0] = 0.0;
	    }
	}

	double value = Math.max(this.forwardProbabilities[0][0], this.forwardProbabilities[1][0]);
	labelmap[0][0] = value;
	if (value == this.forwardProbabilities[0][0]) {
	    labelmap[1][0] = 0;
	} else {
	    labelmap[1][0] = 1;
	}
	labelmap[0][sequence_length] = 0;
	labelmap[1][sequence_length] = 0;

	for (int i = 1; i < this.sequence_length; i++) {
	    for (int q = 0; q < model.getModelStates(); q++) {
		double tmp_sum = 0.0;
		double tmp_sum_zero = 0.0;
		double tmp_sum_one = 0.0;
		for (int r = 0; r < model.getModelStates(); r++) {
		    tmp_sum_zero += this.forwardProbabilities[r][i - 1] * this.maxForwardProbability(q, 0, i, labelmap);
		    tmp_sum_one += this.forwardProbabilities[r][i - 1] * this.maxForwardProbability(q, 1, i, labelmap);
		}
		tmp_sum = Math.max(tmp_sum_zero, tmp_sum_one);
		if (tmp_sum == tmp_sum_zero) {
		    labelmap[0][(q * sequence_length) + i] = tmp_sum_zero;
		    labelmap[1][(q * sequence_length) + i] = 0;
		} else {
		    labelmap[0][(q * sequence_length) + i] = tmp_sum_one;
		    labelmap[1][(q * sequence_length) + i] = 1;
		}
		this.forwardProbabilities[q][i] = tmp_sum;
	    }
	}

	return maxBackward(labelmap);
    }

    private int[] maxBackward(double[][] labelmap) {
	int[] result = new int[sequence_length];
	double init_max = Math.max(labelmap[0][sequence_length - 1], labelmap[0][sequence_length * 2 - 1]);

	if (init_max == labelmap[0][sequence_length - 1]) {
	    result[sequence_length - 1] = (int) labelmap[1][sequence_length - 1];
	} else {
	    result[sequence_length - 1] = (int) labelmap[1][2 * sequence_length - 1];
	}

	for (int i = sequence_length - 1; i > 1; i--) {
	    double forward_zero = this.forwardProbabilities[0][i];
	    double forward_one = this.forwardProbabilities[1][i];
	    double max = Math.max(forward_zero, forward_one);
	    if (max == forward_zero) {
		result[i - 1] = (int) labelmap[1][i];
	    } else {
		result[i - 1] = (int) labelmap[1][sequence_length + i];
	    }
	}
	double zero_max_zero = forwardProbabilities[0][0];
	double zero_max_one = forwardProbabilities[1][0];
	double zero_max = Math.max(zero_max_zero, zero_max_one);
	if (zero_max == zero_max_zero) {
	    result[0] = 0;
	} else {
	    result[0] = 1;
	}
	return result;
    }

    private double maxForwardProbability(int q, int previous_label, int i, double[][] labelmap) {
	double sum = 0.0;
	Tuple<Integer, Integer> current = this.sequence.getEncodedSentence().get(i);
	Tuple<Integer, Integer> previous = this.sequence.getEncodedSentence().get(i - 1);
	double theta_trans = 0.0;
	double theta_prev = 0.0;
	double theta_current = 0.0;
	double theta_prev_label = 0.0;
	// theta(y_i-1,y_i)
	if (this.theta.containsKey(model.getIndex().getIndexOf(0, previous_label, q))) {
	    theta_trans = this.theta.get(model.getIndex().getIndexOf(0, previous_label, q));
	}

	// theta(x_i-1,q)
	if (this.theta
		.containsKey(model.getIndex().getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous.getWord(), q))) {
	    theta_prev = this.theta
		    .get(model.getIndex().getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous.getWord(), q));
	}
	// theta(x_i,q)

	if (this.theta.containsKey(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, current.getWord(), q))) {
	    theta_current = this.theta.get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, current.getWord(), q));
	}

	// theta (x_i-1,y_i-1)
	if (this.theta.containsKey(
		model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, previous.getWord(), previous_label))) {
	    theta_prev_label = this.theta
		    .get(model.getIndex().getIndexOf(Conditions.X_T_AND_NP_T, previous.getWord(), previous_label));
	}

	sum = theta_trans + theta_prev_label + theta_current + theta_prev;
	return Math.exp(sum);
    }
}

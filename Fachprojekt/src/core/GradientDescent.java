package core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import com.sun.corba.se.impl.oa.poa.ActiveObjectMap.Key;

import utils.Conditions;
import utils.Indexing;
import utils.Sentence;
import utils.StaticMath;
import utils.Tuple;
import utils.WordLabelMap;

public class GradientDescent {

    private HashMap<Integer, Double> theta;
    private HashMap<Integer, Double> phi;
    private ArrayList<Sentence> data;
    private final double step_size;
    private final Indexing index;
    private final int modelStates;
    private final int observation_rules;

    public GradientDescent(LinkedHashMap<Integer, Sentence> data, double step_size, WordLabelMap var) {
	Iterator<Sentence> it = data.values().iterator();
	ArrayList<Sentence> data_a = new ArrayList<>();
	while (it.hasNext()) {
	    Sentence sentence = it.next();
	    if (sentence.getEncodedSentence().size() > 0) {
		data_a.add(sentence);
	    }
	}

	this.theta = new HashMap<Integer, Double>();
	this.data = data_a;
	this.step_size = step_size;
	this.index = new Indexing(var, 2, 2);
	this.modelStates = var.get_encoded_labels().size();
	this.observation_rules = 2;

	theta.put(this.index.getIndexOf(0, 0, 0), 0.0);
	theta.put(this.index.getIndexOf(0, 0, 1), 0.0);
	theta.put(this.index.getIndexOf(0, 1, 0), 0.0);
	theta.put(this.index.getIndexOf(0, 1, 1), 0.0);

	for (int i = 1; i <= this.index.getPhi_arity(); i++) {
	    for (int j = 0; j < index.getLabel_arity(); j++) {
		for (int k = 0; k < index.getMapping_size(); k++) {
		    this.theta.put(this.index.getIndexOf(i, k, j), 0.0);
		}
	    }
	}
	this.phi = new HashMap<Integer, Double>(this.theta);
    }

    public void iteration(Conditions[] conditions) {

	Random random = new Random();
	int randomInt = random.nextInt(this.data.size());
	Sentence currentSentence = data.get(randomInt);

	for (Tuple<Integer, Integer> tuple : currentSentence.getEncodedSentence()) {
	    this.phi.replace(index.getIndexOf(Conditions.X_T_AND_NP_T, tuple.getWord(), 0), 0.0);
	    this.phi.replace(index.getIndexOf(Conditions.X_T_AND_NP_T, tuple.getWord(), 1), 0.0);
	    this.phi.replace(index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, tuple.getWord(), 0), 0.0);
	    this.phi.replace(index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, tuple.getWord(), 1), 0.0);
	}

	ForwardBackward probabilites = new ForwardBackward(this, currentSentence);
	probabilites.execute();

	StaticMath.set_phi(Conditions.NP_T_MINUS_1_AND_NP_T, currentSentence, null, index, phi);
	StaticMath.set_phi(Conditions.X_T_AND_NP_T, currentSentence, null, index, phi);
	StaticMath.set_phi(Conditions.X_T_MINUS_1_AND_NP_T, currentSentence, null, index, phi);

	double normalize = 1 / (double) currentSentence.getEncodedSentence().size();
	double normalize_prev = 1 / (double) (currentSentence.getEncodedSentence().size() - 1);
	HashSet<Tuple<Integer, Integer>> vocabulary = new HashSet<>();
	HashSet<Tuple<Integer, Integer>> prev_vocabulary = new HashSet<>();

	for (int i = 0; i < currentSentence.getEncodedSentence().size(); i++) {
	    Tuple<Integer, Integer> current_tuple = currentSentence.getTuple(i);

	    if (i != 0) {
		Tuple<Integer, Integer> previous_tuple = currentSentence.getTuple(i - 1);
		if (prev_vocabulary
			.add(new Tuple<Integer, Integer>(previous_tuple.getWord(), current_tuple.getLabel()))) {
		    int ind = index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous_tuple.getWord(),
			    current_tuple.getLabel());
		    double new_value = phi.get(ind) * normalize_prev;
		    phi.put(ind, new_value);
		}
	    }

	    if (vocabulary.add(current_tuple)) {
		int ind = index.getIndexOf(Conditions.X_T_AND_NP_T, current_tuple.getWord(), current_tuple.getLabel());
		double new_value = phi.get(ind) * normalize;
		phi.put(ind, new_value);
	    }
	}

	for (int pos = 0; pos < currentSentence.getEncodedSentence().size(); pos++) {
	    update(pos, currentSentence, probabilites);
	}
	update(currentSentence, probabilites);
    }

    private void update(Sentence currentSentence, ForwardBackward probabilites) {
	double nnp_nnp = 0.0;
	double nnp_np = 0.0;
	double np_nnp = 0.0;
	double np_np = 0.0;
	if (currentSentence.size() > 1) {
	    for (int i = 1; i < currentSentence.size(); i++) {
		Tuple<Integer, Integer> current = currentSentence.getTuple(i);
		Tuple<Integer, Integer> previous = currentSentence.getTuple(i - 1);
		if (!previous.isNP() && !current.isNP()) {
		    nnp_nnp += this.theta.get(0)
			    * probabilites.getTransitionProbability(i, i - 1, current.getLabel(), previous.getLabel());
		} else if (!previous.isNP() && current.isNP()) {
		    nnp_np += this.theta.get(1)
			    * probabilites.getTransitionProbability(i, i - 1, current.getLabel(), previous.getLabel());
		    ;
		} else if (previous.isNP() && !current.isNP()) {
		    np_nnp += this.theta.get(2)
			    * probabilites.getTransitionProbability(i, i - 1, current.getLabel(), previous.getLabel());
		    ;
		} else if (previous.isNP() && current.isNP()) {
		    np_np += this.theta.get(3)
			    * probabilites.getTransitionProbability(i, i - 1, current.getLabel(), previous.getLabel());
		    ;
		}
	    }
	    nnp_nnp = this.theta.get(0) + this.step_size * (this.phi.get(0) - nnp_nnp);
	    nnp_np = this.theta.get(1) + this.step_size * (this.phi.get(1) - nnp_np);
	    np_nnp = this.theta.get(2) + this.step_size * (this.phi.get(2) - np_nnp);
	    np_np = this.theta.get(3) + this.step_size * (this.phi.get(3) - np_np);
	    this.theta.put(0, nnp_nnp);
	    this.theta.put(1, nnp_np);
	    this.theta.put(2, np_nnp);
	    this.theta.put(3, np_np);
	}

    }

    private void update(int position, Sentence currentSentence, ForwardBackward prob) {
	double[][] probabilites = prob.getProbabilities();
	Tuple<Integer, Integer> tuple = currentSentence.getTuple(position);

	/*
	 * Update The Word, Label combination for both NP and NNP
	 */
	int np_key = index.getIndexOf(Conditions.X_T_AND_NP_T, tuple.getWord(), 1);
	double theta_old_np = this.theta.get(np_key);
	double phi_np = this.phi.get(np_key);
	double theta_new_np = theta_old_np + step_size * (phi_np - (theta_old_np * probabilites[1][position]));
	this.theta.put(np_key, theta_new_np);

	int nnp_key = index.getIndexOf(Conditions.X_T_AND_NP_T, tuple.getWord(), 0);
	double theta_old_nnp = this.theta.get(nnp_key);
	double phi_nnp = this.phi.get(nnp_key);
	double theta_new_nnp = theta_old_nnp + step_size * (phi_nnp - (theta_old_nnp * probabilites[0][position]));
	this.theta.put(nnp_key, theta_new_nnp);

	/*
	 * Update the Label-Label transitions and the previous Word-Label
	 * transitions
	 */
	if (position != 0) {
	    Tuple<Integer, Integer> previous = currentSentence.getTuple(position - 1);
	    int previous_np_key = index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous.getWord(), 1);
	    int previous_nnp_key = index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous.getWord(), 0);

	    double previous_theta_old_np = this.theta.get(previous_np_key);
	    double previous_phi_np = this.phi.get(previous_np_key);
	    double previous_theta_new_np = previous_theta_old_np
		    + step_size * (previous_phi_np - (previous_theta_old_np * probabilites[1][position]));

	    double previous_theta_old_nnp = this.theta.get(previous_nnp_key);
	    double previous_phi_nnp = this.phi.get(previous_nnp_key);
	    double previous_theta_new_nnp = previous_theta_old_nnp
		    + step_size * (previous_phi_nnp - (previous_theta_old_nnp * probabilites[0][position]));

	    this.theta.put(previous_np_key, previous_theta_new_np);
	    this.theta.put(previous_nnp_key, previous_theta_new_nnp);

	}

    }

    public HashMap<Integer, Double> getModel() {
	return this.theta;
    }

    public void setModel(HashMap<Integer, Double> model) {
	this.theta = model;
    }

    public ArrayList<Sentence> getData() {
	return data;
    }

    public void setData(ArrayList<Sentence> data) {
	this.data = data;
    }

    public double getStepSize() {
	return step_size;
    }

    public HashMap<Integer, Double> getPhi() {
	return this.phi;
    }

    public HashMap<Integer, Double> getTheta() {
	return theta;
    }

    public void setTheta(HashMap<Integer, Double> theta) {
	this.theta = theta;
    }

    public double getEta() {
	return step_size;
    }

    public Indexing getIndex() {
	return index;
    }

    public int getModelStates() {
	return modelStates;
    }

    public void setPhi(HashMap<Integer, Double> phi) {
	this.phi = phi;
    }

    public int getObservation_rules() {
	return observation_rules;
    }
}

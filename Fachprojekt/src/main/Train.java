package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;

import core.GradientDescent;
import utils.Conditions;
import utils.Sentence;
import utils.Tuple;
import utils.WordLabelMap;

public class Train {

    public static void main(String args[]) throws IOException {
	String path = "G:\\workspace\\Fachprojekt\\data\\train.txt";
	BufferedReader br = new BufferedReader(new FileReader(path));

	String line;
	int i = 0;
	ArrayList<Sentence> text = new ArrayList<>();
	LinkedHashSet<Tuple<String, String>> mapping = new LinkedHashSet<>();
	HashMap<String, Integer> statistics = new HashMap<>();
	text.add(new Sentence());
	int currentSentence = 0;
	while ((line = br.readLine()) != null) {
	    StringTokenizer st = new StringTokenizer(line);

	    if (st.hasMoreTokens()) {
		String word = st.nextToken();
		st.nextToken();
		String label = st.nextToken();
		if (label.equals("B-NP") || label.equals("I-NP")) {
		    label = "NP";
		} else {
		    label = "NNP";
		}
		word = word.toLowerCase();
		mapping.add(new Tuple<String, String>(word, label));

		if (statistics.containsKey(word)) {
		    i = statistics.get(word);
		    statistics.put(word, i + 1);
		} else {
		    statistics.put(word, 1);
		}
		text.get(currentSentence).addData(word, label);
	    } else {
		currentSentence++;
		text.add(new Sentence());
	    }

	    i++;
	}

	br.close();
	/**
	 * 
	 */

	BufferedReader br_two = new BufferedReader(new FileReader("G:\\workspace\\Fachprojekt\\data\\test.txt"));
	i = 0;
	while ((line = br_two.readLine()) != null) {
	    StringTokenizer st = new StringTokenizer(line);

	    if (st.hasMoreTokens()) {
		String word = st.nextToken();
		st.nextToken();
		String label = st.nextToken();
		if (label.equals("B-NP") || label.equals("I-NP")) {
		    label = "NP";
		} else {
		    label = "NNP";
		}
		word = word.toLowerCase();
		mapping.add(new Tuple<String, String>(word, label));
	    }
	}
	br_two.close();
	/*
	 * Initial Mapping of the text
	 */
	final WordLabelMap var = new WordLabelMap(mapping);
	LinkedHashMap<Integer, Sentence> encoded_text_map = new LinkedHashMap<>();
	int k = 0;
	for (Sentence sentence : text) {
	    System.out.println(sentence.toString());
	    encoded_text_map.put(k, sentence);
	    for (Tuple<String, String> tuple : sentence.getSentence()) {
		sentence.addEncodedData(var.get_code_word(tuple.getWord()), var.get_code_label(tuple.getLabel()));

	    }
	    k++;
	}
	/**
	 * Create the model for the given mapping
	 */
	GradientDescent gd = new GradientDescent(encoded_text_map, 1e-2, var);

	Conditions[] cond = { Conditions.NP_T_MINUS_1_AND_NP_T, Conditions.X_T_AND_NP_T,
		Conditions.X_T_MINUS_1_AND_NP_T };
	int iteratio = 20000;
	HashMap<Integer, Double> theta_max = new HashMap<>();
	double ratio = 0.0;
	for (int q = 0; q < iteratio; q++) {
	    gd.iteration(cond);
	    if (q % (50) == 0) {
		Test test = new Test(gd, var);
		double new_ratio = test.runTest();
		if (new_ratio > ratio) {
		    theta_max = new HashMap<>(gd.getTheta());
		    ratio = new_ratio;
		}
	    }
	}

	gd.setTheta(theta_max);
	Test test = new Test(gd, var);
	test.runTest();
	System.exit(0);
    }
}
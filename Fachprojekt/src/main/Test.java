package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;

import core.ForwardBackward;
import core.GradientDescent;
import utils.Sentence;
import utils.Tuple;
import utils.WordLabelMap;

public class Test {

    private GradientDescent model;
    private WordLabelMap var;

    public Test(GradientDescent gd, WordLabelMap var) {
	this.model = gd;
	this.var = var;
    }

    public double runTest() throws IOException {
	String path = "G:\\workspace\\Fachprojekt\\data\\test.txt";
	BufferedReader br = new BufferedReader(new FileReader(path));

	String line;
	int i = 0;
	ArrayList<Sentence> text = new ArrayList<>();
	LinkedHashSet<Tuple<String, String>> mapping = new LinkedHashSet<>();
	HashMap<String, Integer> statistics = new HashMap<>();
	text.add(new Sentence());
	int currentSentence = 0;
	while ((line = br.readLine()) != null) {
	    StringTokenizer st = new StringTokenizer(line);

	    if (st.hasMoreTokens()) {
		String word = st.nextToken();
		st.nextToken();
		String label = st.nextToken();
		if (label.equals("B-NP") || label.equals("I-NP")) {
		    label = "NP";
		} else {
		    label = "NNP";
		}
		word = word.toLowerCase();
		mapping.add(new Tuple<String, String>(word, label));

		if (statistics.containsKey(word)) {
		    i = statistics.get(word);
		    statistics.put(word, i + 1);
		} else {
		    statistics.put(word, 1);
		}
		text.get(currentSentence).addData(word, label);
	    } else {
		currentSentence++;
		text.add(new Sentence());
	    }
	    i++;
	}

	LinkedHashMap<Integer, Sentence> encoded_text_map = new LinkedHashMap<>();
	int k = 0;
	for (Sentence sentence : text) {
	    encoded_text_map.put(k, sentence);
	    for (Tuple<String, String> tuple : sentence.getSentence()) {
		sentence.addEncodedData(var.get_code_word(tuple.getWord()), var.get_code_label(tuple.getLabel()));

	    }
	    k++;
	}
	ArrayList<int[]> labels = new ArrayList<>();
	for (Sentence sentence : encoded_text_map.values()) {
	    if (sentence.size() > 0) {
		ForwardBackward fdw = new ForwardBackward(model, sentence);
		fdw.set_Phi_Theta();
		labels.add(fdw.maxForward());
	    }
	}

	double correct = 0;
	double true_negative = 0;
	double true_positive = 0;
	double false_positive = 0;
	double false_negative = 0;
	double wrong = 0;
	double count = 0;
	double np = 0;
	double nnp = 0;
	for (int i1 = 0; i1 < encoded_text_map.size(); i1++) {
	    Sentence sentence = encoded_text_map.get(i1);
	    for (int j = 0; j < sentence.size(); j++) {
		int prediction = labels.get(i1)[j];
		if (sentence.getEncodedSentence().get(j).getLabel() == 0) {
		    nnp++;
		} else if (sentence.getEncodedSentence().get(j).getLabel() == 1) {
		    np++;
		}
		if (prediction == sentence.getEncodedSentence().get(j).getLabel()) {
		    if (prediction == 0) {
			true_negative++;
		    } else {
			true_positive++;
		    }
		    correct++;
		} else {
		    if (prediction == 1 && sentence.getEncodedSentence().get(j).getLabel() == 0) {
			false_positive++;
		    } else if (prediction == 0 && sentence.getEncodedSentence().get(j).getLabel() == 1) {
			false_negative++;
		    }
		    wrong++;
		}
		count++;
	    }
	}
	double precision = true_positive / (true_positive + false_positive);
	double recall = true_positive / (true_positive + false_negative);
	double tpr = true_positive / (true_positive + false_negative);
	double fnr = false_negative / (false_negative + true_positive);
	double fpr = false_positive / (false_positive + true_negative);
	double tnr = true_negative / (true_negative + false_positive);

	System.out.println("NP: " + np / (np + nnp));
	System.out.println("NNP: " + nnp / (np + nnp));
	System.out.println("CORRECT: " + correct / count);
	System.out.println("WRONG: " + wrong / count);
	System.out.println("TRUE POSITIVE RATE : " + tpr);
	System.out.println("FALSE NEGATIVE RATE : " + fnr);
	System.out.println("TRUE NEGATIVE RATE : " + tnr);
	System.out.println("FALSE POSITIVE RATE : " + fpr);
	System.out.println("PRECISION : " + precision);
	System.out.println("RECALL : " + recall);
	
	return correct/count;
    }
}

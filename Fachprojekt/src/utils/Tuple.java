package utils;

public class Tuple<S1, S2> {

    private S1 word;
    private S2 label;

    public Tuple(S1 word, S2 label) {
	this.word = word;
	this.label = label;
    }

    public S1 getWord() {
	return word;
    }

    public void setWord(S1 word) {
	this.word = word;
    }

    public S2 getLabel() {
	return label;
    }

    public void setLabel(S2 label) {
	this.label = label;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((label == null) ? 0 : label.hashCode());
	result = prime * result + ((word == null) ? 0 : word.hashCode());
	return result;
    }

    public boolean isNP() {
	return (this.label.equals(1));
    }

    public int getComplementLabel() {
	if (this.label.equals(1))
	    return 0;
	return 1;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Tuple<?, ?> other = (Tuple<?, ?>) obj;
	if (label == null) {
	    if (other.label != null)
		return false;
	} else if (!label.equals(other.label))
	    return false;
	if (word == null) {
	    if (other.word != null)
		return false;
	} else if (!word.equals(other.word))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "Tuple [word=" + word + ", label=" + label + "]";
    }

}

package utils;

import java.util.ArrayList;

public class Sentence {

    private ArrayList<Tuple<String, String>> data;
    private ArrayList<Tuple<Integer, Integer>> encoded_data;

    public Sentence() {
	this.data = new ArrayList<>();
	this.encoded_data = new ArrayList<>();
    }

    public void addData(String word, String label) {
	this.data.add(new Tuple<String, String>(word, label));
    }

    public void addEncodedData(Integer word, Integer label) {
	this.encoded_data.add(new Tuple<Integer, Integer>(word, label));
    }

    public ArrayList<Tuple<Integer, Integer>> getEncodedSentence() {
	return this.encoded_data;
    }

    @Override
    public String toString() {
	String result = "";
	for (Tuple<String, String> tuple : this.data) {
	    result += tuple.getWord() + " ";
	}
	return result;
    }

    public ArrayList<Tuple<String, String>> getSentence() {
	return this.data;
    }

    public Tuple<Integer, Integer> getTuple(int position) {
	return this.encoded_data.get(position);
    }

    public int size() {
	return this.encoded_data.size();
    }

}

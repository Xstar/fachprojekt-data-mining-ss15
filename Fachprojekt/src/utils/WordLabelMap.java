package utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

public class WordLabelMap {

    ArrayList<Integer> code;

    HashBiMap<Integer, String> encode_words;
    HashBiMap<Integer, String> encode_labels;

    public WordLabelMap(LinkedHashSet<Tuple<String, String>> mapping) {
	encode_words = HashBiMap.create();
	encode_labels = HashBiMap.create();
	int i = 0;
	for (Tuple<String, String> tuple : mapping) {
	    if (this.encode_words.containsValue(tuple.getWord()) == false) {
		this.encode_words.put(i, (String) tuple.getWord());
		i++;
	    }
	}
	encode_labels.put(0, "NNP");
	encode_labels.put(1, "NP");
    }

    public BiMap<Integer, String> get_encoded_words() {
	return this.encode_words;
    }

    public BiMap<Integer, String> get_encoded_labels() {
	return this.encode_labels;
    }

    public int get_code_word(String word) {
	return this.encode_words.inverse().get(word);
    }

    public int get_code_label(String label) {
	return this.encode_labels.inverse().get(label);
    }

    public String get_word(int word_code) {
	return this.encode_words.get(word_code);
    }

    public String get_label(int label_code) {
	return this.encode_labels.get(label_code);
    }
}

package utils;

import java.util.HashMap;

/**
 * @author Dannz_Heinrich
 */

public class StaticMath {

    /**
     * Computes the probabilty vector phi by iterating over the given data with
     * an encoding.
     * 
     * @param text
     *            The data on which phi shall be computed
     * @param current
     *            the current element , for which phi should be computed
     * @param phi
     * @param the
     *            given encoding for the dictionary
     * @return a vector
     */
    public static void set_phi(Conditions condition, Sentence sentence, Tuple<Integer, Integer> current, Indexing index,
	    HashMap<Integer, Double> phi) {

	double normalize_label = 1 / (double) (sentence.getEncodedSentence().size() - 1);

	if (condition == Conditions.X_T_AND_NP_T) {
	    for (int i = 0; i < sentence.getEncodedSentence().size(); i++) {
		Tuple<Integer, Integer> current_tuple = sentence.getTuple(i);
		int ind = index.getIndexOf(Conditions.X_T_AND_NP_T, current_tuple.getWord(), current_tuple.getLabel());
		double new_value = phi.get(ind) + 1.0;
		phi.put(ind, new_value);
	    }
	} else if (condition == Conditions.X_T_MINUS_1_AND_NP_T) {
	    for (int i = 1; i < sentence.getEncodedSentence().size(); i++) {
		Tuple<Integer, Integer> previous_tuple = sentence.getTuple(i - 1);
		Tuple<Integer, Integer> current_tuple = sentence.getTuple(i);
		int ind = index.getIndexOf(Conditions.X_T_MINUS_1_AND_NP_T, previous_tuple.getWord(),
			current_tuple.getLabel());
		double new_value = phi.get(ind) + 1.0;
		phi.put(ind, new_value);
	    }
	} else if (condition == Conditions.NP_T_MINUS_1_AND_NP_T) {

	    double notnp_notnp = 0, notnp_np = 0, np_notnp = 0, np_np = 0;
	    for (int i = 1; i < sentence.getEncodedSentence().size(); i++) {
		Tuple<Integer, Integer> tuple = sentence.getEncodedSentence().get(i);
		Tuple<Integer, Integer> previous_tuple = sentence.getEncodedSentence().get(i - 1);

		if (!previous_tuple.isNP() && !tuple.isNP()) {
		    notnp_notnp++;
		} else if (!previous_tuple.isNP() && tuple.isNP()) {
		    notnp_np++;
		} else if (previous_tuple.isNP() && !tuple.isNP()) {
		    np_notnp++;
		} else if (previous_tuple.isNP() && tuple.isNP()) {
		    np_np++;
		}
	    }
	    phi.put(0, normalize_label * notnp_notnp);
	    phi.put(1, normalize_label * notnp_np);
	    phi.put(2, normalize_label * np_notnp);
	    phi.put(3, normalize_label * np_np);
	}
    }
}

package utils;

public class Indexing {

    WordLabelMap mapping;
    private final int mapping_size;
    private final int index_size;
    private final int phi_arity;
    private final int label_arity;

    public Indexing(WordLabelMap mapping, int phi, int labels) {
	this.mapping = mapping;
	this.mapping_size = mapping.get_encoded_words().size();

	this.phi_arity = phi;
	this.label_arity = labels;
	this.index_size = this.getIndexOf(phi, mapping.encode_words.size() - 1, labels - 1);
    }

    public WordLabelMap getMapping() {
	return this.mapping;
    }

    public int getIndexOfString(int phi, String word, int label) {
	return getIndexOf(phi, this.mapping.get_code_word(word), label);
    }

    public int getIndexOf(Conditions condition, int word, int label) {
	if (condition.equals(Conditions.NP_T_MINUS_1_AND_NP_T)) {
	    return this.getIndexOf(0, word, label);
	} else if (condition.equals(Conditions.X_T_AND_NP_T)) {
	    return this.getIndexOf(1, word, label);
	} else if (condition.equals(Conditions.X_T_MINUS_1_AND_NP_T)) {
	    return this.getIndexOf(2, word, label);
	}
	return -1;
    }

    public int getIndexOf(int phi, int word, int label) {
	if (word > this.mapping_size - 1) {
	    System.out.println("ERROR index of word greater than mapping size" + word);
	    return -1;
	}
	if (phi == 0) {
	    if (word == 0 && label == 0) {
		return 0;
	    }
	    if (word == 0 && label == 1) {
		return 1;
	    }
	    if (word == 1 && label == 0) {
		return 2;
	    }
	    if (word == 1 && label == 1) {
		return 3;
	    }
	} else if (phi == 1) {
	    if (label == 0) {
		return word + 4;
	    } else {
		return phi * this.mapping_size + word + 4;
	    }
	} else if (phi == 2) {
	    if (label == 0) {
		return (label + phi) * this.mapping_size + word + 4;
	    } else {
		return (label + phi) * this.mapping_size + word + 4;
	    }
	} else {
	    System.out.println("ERROR index of phi greater than phi size");
	    return -1;
	}
	System.out.println("ERROR " + phi + " " + word + " " + label);
	return -1;
    }

    public int getMapping_size() {
	return mapping_size;
    }

    public int getIndex_size() {
	return index_size;
    }

    public int getPhi_arity() {
	return phi_arity;
    }

    public int getLabel_arity() {
	return label_arity;
    }
}
